# WarFairy "Charon" AR-15 Receiver V0.1B

### WARNING: 
+ Printing this receiver constitutes constructing a firearm. Check all local laws prior to printing.
+ This is a Beta Release. These parts are absolutely 100% untested. Print or manufacture at your own risk.

### Disclaimer

I am NOT an engineer. I hold no degree. I claim no advanced education. Take from that what you will.

Thank you for downloading this Receiver. In this Readme, you will find instructions on the construction and assembly of the receiver.

Please note, this file has been designed with FDM Printing in mind. All parts except the Butt Plate should be printed from Rear to Front. The Butt plate should be printed Front to Rear. Warping issues have been noted on a prior release when printing with the stock laying on its side. Remember, this is a firearm. Use whatever infill setting you wish, but it is HIGHLY recommended that you use 100% for the Receiver Middle and Front sections.

### You Will Need
+ A printer with a print envelope at least 6"x6"x6".
+ Sand Paper (600 grit for coarse, 800-1000 grit for finishing)
+ Glue (Our resident print wizard suggests Weld-On #3)
+ 2.21 lbs of filament, not including your selected Fore End (at 100% infill). Add approximately .5 lb if printing one of the grips, .1 for the cap
+ A Lower Parts Kit for an AR-15
+ The Action Spring (AKA Buffer Spring) for an AR-15
+ A carbine size Buffer

### You Will Need to Print the Following

+ Receiver Front x1
+ Receiver Middle x1
+ Receiver Rear x1
+ Buffer Retainer Tube Plug x1
+ Butt Cover x1
+ Butt Pad x1
+ Foregrip Pin x1
+ Rear Takedown Pin x1
+ Stock Pegs x6

####One of the following (REQUIRED):
+ Smooth Face Block
+ Smooth Fore Grip Block
+ Smooth Fore Grip No Guard Block
+ Tactical Forearm Block

### Optional
Spacer (This goes between the Butt Pad and Butt Cover, adding 10mm in length (about.4") to adjust stock length)


### Assembly

1. Assemble Receiver Sections by pressing them into position, using the knobs and cutouts as guides. Ensure assembled sections fit smoothly, that the FCG pocket is well aligned, and that the Buffer Tube is aligned well. Once fit is verified, disassemble and use the glue on the flat interfaces between the three sections.

2. Put FCG into place. The Safety detent and Safety detent spring will need to be dropped through the hole in the top of the receiver over the Safety's hole. Use care not to bend the pin when inserting the Safety switch.

3. Function check your FCG and adjust as necessary for reliable function.

4. Insert Buffer Retainer ahead of Buffer Retainer Spring through the hole in the upper surface of the Thumbhole stock. Insert the Buffer Retainer Tube Plug so that the contour on the bottom of the plug matches the surface of the Thumb Hole. Use a small amount of glue on the Buffer Retainer Tube Plug to keep in place.

5. Insert Action Spring and Buffer into Buffer Tube. Ensure that buffer slides all the way to the rear without binding.

6. Assemble Bolt Release and Magazine release. Ensure bolt release moves relatively freely and that the plunger does not bind. Ensure magazine release does not bind.

7. Assemble Front Take Down Pin assembly.

8. Press your chosen Forward piece into place by inserting through the slots from the bottom of the receiver until the pin holes align. Press Foregrip Pin through the hole until it is flush on one side. It should be slightly long. Sand to fit.

9. Assemble Butt Cover. Refer to included Stock Assembly Helper.JPG

10. Press Butt Cover Assembly into place on the rear of the Stock, taking care to alight the two pegs and six rails with their respective openings.

11. ALLOW ALL GLUE TO CURE FOR AT LEAST 24 HOURS BEFORE FIRING.

12. Attach your chosen Upper Receiver to front Takedown pin and rotate into place. Press Rear Takedown Pin through hole in stock until it is flush with opposite side.



### Post Notes

A later release will include my Logo. Having that complex little lady on either side of the stock slowed my computer to a standstill when I tried to convert to STEP and STL. I've got to optimize her heavilly before she can be included on a larger piece.

I'd like to thank all of the kind people on the #defcad IRC channel for their support and suggestions, especially my Beta Tester. You know who you are, and you're doing great things, man. Y'all have kept me sane for the past few months.

I'd also like to thank the good lads and ladies over at /k/. Y'all are brutally honest when it comes to critique, and I needed that. Keep doin what you do.

If you'd like to contact me, the #defcad IRC is where I'm usually lurking. You may also contact me by email at shanrilivan@yahoo.com. If you have any gripes, complaints, suggestions or praise, you know how to find me.
