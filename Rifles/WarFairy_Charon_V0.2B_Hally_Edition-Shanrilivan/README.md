#The WarFairy Charon (Hally's Edition)

### Warning: 

This is a bufferless AR pistol. Attempting to use an upper that requires a buffer will result in catastrophic failure. If you don't know what the difference is between a buffered and unbuffered AR is: If the bolt carrier is very close to the rear of the upper when it is fully forward, it is a buffered upper. Do not attempt to use these kinds of uppers on this pistol.

All standard warnings apply. You're on your own.

### You will need:
* Glue
* 1.1 Pounds of ABS Filament (Will vary if using different Material)
* 30x 10mm sections of 1.8mm Filament

### Instructions 
Print Mid and Front sections from butt to muzzle. Print Rear section muzzle to butt.
When printed, apply glue to each hole in the front and rear sections just before inserting the 10mm section of filament into each hole.
When all pegs are in place, apply glue to all holes in rear of mid section and guide onto pegs until the faces of the two parts meet. Do the same for the front once rear is in place.

Allow glue to cure for 24 hours before firing.

### Recommended uppers:
Rock River Arms LAR-PDS (Upper not sold seperately, firearm costs USD$1250 according to manufacturer)
Faxon Arms ARAK-21 (Only sold in 16" or longer barrel. These are not legal to use on a pistol lower in the US without a tax stamp)
Olympic Arms OA-93 (No longer in production. Keep an eye on forums and auction sites.)
